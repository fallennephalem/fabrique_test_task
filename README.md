# Fabrique_test_task

## Download and setup

First as first clone repository:  
`git clone https://gitlab.com/fallennephalem/fabrique_test_task/`  

Than install requirements:  
`pip install -r requirements.txt`  

Don't forget about enviroments varibles. Paste `setenv.sh` file and start it:  
`bash .\setenv.sh`  

Move to the directory with `manage.py` file and start commands:  
`python maange.py makemigrations`  
`python manage.py migrate`

## Running  

In the directory with `manage.py` file run command:  
`python manage.py runserver`

If you need information about API urls, documentation about allowed methods are here:  
`http://127.0.0.1:8000/swagger/`

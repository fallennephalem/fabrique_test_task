
from django.apps import AppConfig
from apscheduler.schedulers.background import BackgroundScheduler


class MaillingListConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mailling_list'
    scheduler = BackgroundScheduler({
    'apscheduler.executors.default': {
        'class': 'apscheduler.executors.pool:ThreadPoolExecutor',
        'max_workers': '20'
    },
    'apscheduler.executors.processpool': {
        'type': 'processpool',
        'max_workers': '5'
    },
    'apscheduler.job_defaults.coalesce': 'false',
    'apscheduler.job_defaults.max_instances': '3',
    'apscheduler.timezone': 'UTC',
})

    def ready(self):
        self.scheduler.start()

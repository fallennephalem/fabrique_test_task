import os
import requests
from datetime import datetime
from requests.structures import CaseInsensitiveDict
from .models import Message, Mail_statistic


 
api_endpoint = os.environ.get('API_ENDPOINT')
api_jwt_token = os.environ.get('API_JWT_TOKEN')

 
def is_time(start_time, end_time):
    start_time = datetime.strptime(start_time, '%Y-%m-%dT%H:%M:%SZ')
    end_time = datetime.strptime(end_time, '%Y-%m-%dT%H:%M:%SZ')
    return [start_time < datetime.utcnow() < end_time, end_time > datetime.utcnow()]
 
 
def send_mail(json_data, clients, mail):
    headers = CaseInsensitiveDict({
        'Authorization': f'Bearer {api_jwt_token}',
        'Content-Type': 'application/json'
    })

 
    statistics = Mail_statistic.objects.filter(mail__pk=mail.id).first()
    if statistics:
        return statistics

    sent_messages = 0
    for client in clients:
        new_message = Message(status=False, mail=mail, client=client)
        new_message.save()
        resp = requests.post(
            f'{api_endpoint}/{new_message.id}',
            headers=headers,
            json={
                'id': new_message.id,
                'phone': client.phone_number,
                'text': mail.text
            },
            timeout=5
        )
        if resp.status_code == 200:
            new_message.status = True
            sent_messages += 1
        new_message.save()

    statistics = {
        'messages_in_mail': len(clients),
        'sent_successfully': sent_messages,
        'got_error': len(clients) - sent_messages
    }
    Mail_statistic(statistics=statistics, mail=mail).save()

    return statistics


from email.policy import default
from django.db import models
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from .utils import *
import re, pytz

TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
# Create your models here.
filters_template = {"mobile_code" : "", "tags":""}
def default_filters():
    return filters_template

def filters_validator(filters):
    if filters is None:
        raise ValidationError('Filters must be in the form of such template: {0}'.format(filters_template))
    if any((len(filters) != 2, 'mobile_code' not in filters, 'tags' not in filters)):
        raise ValidationError('Filters must be in the form of such template: {0}'.format(filters_template))

class Mail(models.Model):
    start_datetime = models.DateTimeField(validators=[check_comparison_with_template_datetime])
    text = models.TextField()
    filters = models.JSONField(validators=[filters_validator], default=default_filters)
    end_datetime = models.DateTimeField(validators=[check_comparison_with_template_datetime, end_datetime_validator])

    def __str__(self):
        return self.text

class Client(models.Model):
    phone_regex = RegexValidator(regex=r'7\d{10}', message="Phone number must be entered in the format: '7XXXXXXXXXX'.")
    phone_number = models.CharField(validators=[phone_regex], max_length=11, blank=True)
    operator_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=32)
    timezone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC')

class Message(models.Model):
    creation_datetime = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False)
    mail = models.ForeignKey('mail', on_delete=models.CASCADE)
    client = models.ForeignKey('client', on_delete=models.CASCADE)

class Mail_statistic(models.Model):
    statistics = models.JSONField()
    mail = models.ForeignKey('mail', on_delete=models.CASCADE)

from rest_framework import serializers
from .models import Client, Mail, Message, Mail_statistic


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('__all__')

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('__all__')

class StatisticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mail_statistic
        fields = ('statistics', 'mail')

# example: {"start_datetime": "2022-10-03T12:54:51+03:00", "text": "fnsdbjsd", "filters":{"mobile_code":"888","tags":"tank"}, "end_datetime":"2022-10-29T23:20:52+03:00"}
class MailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mail
        fields = ('__all__')
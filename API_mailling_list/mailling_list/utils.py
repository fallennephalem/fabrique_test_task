from django.core.exceptions import ValidationError
from django.utils import timezone
import re

filters_template = {"mobile_code" : "", "tags":""}
def default_filters():
    return filters_template

def filters_validator(filters):
    if filters is None:
        raise ValidationError('Filters must be in the form of such template: {0}'.format(filters_template))
    if any((len(filters) != 2, 'mobile_code' not in filters, 'tags' not in filters)):
        raise ValidationError('Filters must be in the form of such template: {0}'.format(filters_template))

def end_datetime_validator(end_time):
    if end_time < timezone.now():
        raise ValidationError('end_datatime is less that now.')

def check_comparison_with_template_datetime(time):
    str_time = time.strftime('%Y-%m-%dT%H:%M:%S%z')
    template1 = r'\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[\+-]\d{2}\d{2}'
    template2 = r'\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z'
    if not (re.fullmatch(template1, str_time) or re.fullmatch(template2, str_time)):
        raise ValidationError('Time must be in template: %Y-%m-%dT%H:%M:%S%z')

from django.urls import path
from .views import CreateClient, ClientDetail, CreateMail, MailStatistics, MailStatisticsList, MailDetails

urlpatterns = [
    path('create_client/', CreateClient.as_view()),
    path('client_details/<int:pk>', ClientDetail.as_view()),
    path('create_mail/', CreateMail.as_view()),
    path('mail_details/<int:pk>', MailDetails.as_view()),
    path('mail_statistics/<int:pk>', MailStatistics.as_view()),
    path('mail_statistics_list/', MailStatisticsList.as_view())
]
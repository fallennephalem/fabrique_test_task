# Generated by Django 4.1.1 on 2022-09-21 10:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mailling_list', '0006_alter_mail_end_datetime_alter_mail_start_datetime'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='status',
            field=models.BooleanField(default=False),
        ),
    ]

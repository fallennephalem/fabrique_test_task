from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status

from .models import Mail, Client, Mail_statistic
from .serializers import MailSerializer, ClientSerializer, StatisticsSerializer
from .mail_driver import send_mail
from .apps import MaillingListConfig
from datetime import datetime
from datetime import timedelta
from django.utils import timezone

def serialize_and_try_to_sent(serializer):
    serializer.is_valid(raise_exception=True)
    new_mail = serializer.save()
    operator_code = serializer.data['filters']['mobile_code']
    tag = serializer.data['filters']['tags']
    start_time = datetime.strptime(serializer.data['start_datetime'], '%Y-%m-%dT%H:%M:%S%z')
    statistics = send_mail(serializer.data, Client.objects.filter(operator_code=operator_code, tag=tag), new_mail)
    return [statistics, new_mail, operator_code, tag, start_time]

class CreateClient(generics.CreateAPIView):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

class ClientDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

class MailStatisticsList(generics.ListAPIView):
    serializer_class = StatisticsSerializer
    queryset = Mail_statistic.objects.all()

class MailStatistics(generics.RetrieveAPIView):
    serializer_class = StatisticsSerializer
    queryset = Mail_statistic.objects.all()

    def get(self, request, *args, **kwargs):
        if len(Mail.objects.get(id=self.kwargs['pk']).mail_statistic_set.all()) == 0:
            return Response({'error' : 'No such statistics'})
        mail_statistics_instance = Mail.objects.get(id=self.kwargs['pk']).mail_statistic_set.all()[0]
        return Response(mail_statistics_instance.statistics)

class CreateMail(generics.CreateAPIView):
    '''Mail creating. (write id in url for change mail)\n
    Example: {"start_datetime": "2022-10-03T12:54:51+03:00", "text": "fnsdbjsd", "filters":{"mobile_code":"888","tags":"tank"}, "end_datetime":"2022-10-29T23:20:52+03:00"}'''
    serializer_class = MailSerializer
    queryset = Mail_statistic.objects.all()
    def post(self, request):
        serializer = MailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        run_date = instance.start_datetime
        if run_date <= timezone.now(): run_date = timezone.now()+timedelta(seconds=5)
        MaillingListConfig.scheduler.add_job(
            send_mail,
            'date',
            run_date=run_date,
            id=f'mail_{instance.pk}',
            replace_existing=True,
            kwargs={
                'json_data': serializer.data,
                'clients': Client.objects.filter(
                    operator_code=instance.filters['mobile_code'],
                    tag=instance.filters['tags']
                ),
                'mail': instance
            })


        return Response({'id' : instance.id})

class MailDetails(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = MailSerializer
    queryset = Mail.objects.all()
    def put(self, request, *args, **kwargs):
        
        instance = Mail.objects.filter(pk=kwargs.get('pk')).first()
        if not instance:
            return Response({'error': 'Mail does not exists'}, status=status.HTTP_404_NOT_FOUND)

        serializer = MailSerializer(data=request.data, instance=instance)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        if MaillingListConfig.scheduler.get_job(f'mail_{instance.id}'):
            job = MaillingListConfig.scheduler.modify_job(f'mail_{instance.id}', next_run_time=instance.start_datetime,
             kwargs={
                'json_data': serializer.data,
                'clients': Client.objects.filter(
                    operator_code=instance.filters['mobile_code'],
                    tag=instance.filters['tags']),
                'mail': instance
            })
        
            if instance.start_datetime < timezone.now():
                job.modify(next_run_time=timezone.now()+timedelta(seconds=5))
        return Response({'instance' : serializer.data})

    


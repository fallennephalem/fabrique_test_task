from django.contrib import admin
from .models import Mail, Client, Message, Mail_statistic

# Register your models here.
admin.site.register(Mail)
admin.site.register(Client)
admin.site.register(Message)
admin.site.register(Mail_statistic)